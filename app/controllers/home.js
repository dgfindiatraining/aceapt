var Api = require('Api');
var mainDB = Alloy.Collections.mainDB;

var dropdownContent = require("Dropdown"),
    clickListner = {
	event : 'click',
	cbFunc : ''
},
    longpressListner = {
	event : 'longpress',
	cbFunc : ''
};

clickListner.cbFunc = function() {
	alert("something something something");
};
longpressListner.cbFunc = function() {
	alert("nothing nothing nothing");
};

// return an array of models
function whereFunction(collection) {

}

// Initialize everything
(function init() {
	//Init slider widget
	$.widg.initWidget([clickListner, longpressListner], [dropdownContent.getDropdownIndustry(), dropdownContent.getDropdownProducts()]);

	// Init Activity indicator Widget
	$.activityInd.construct({
		bottom : 200
	});
	$.activityInd.show();
	$.activityInd.updateTitle({
		title : 'Fetching\nProducts Data'
	});

	// test code
	Api.getProducts(function(data) {
		var model = Alloy.createModel('mainDB', {
			"type" : "Product",
			"category" : data.response[0].category,
			"preferred" : data.response[0].preferred,
			"standard" : data.response[0].standard,
			"restricted" : data.response[0].restricted,
			"productsAvailable" : data.response[0].productsAvailable
		});

		mainDB.add(model);

	}, function(error) {
		Ti.API.info("Api.getProducts error: " + JSON.stringify(error));
	});

	Api.getIndustries(function(data) {
		var model = Alloy.createModel('mainDB', {
			"type" : "Indusrtries",
			"category" : data.response[0].category,
			"preferred" : data.response[0].preferred,
			"standard" : data.response[0].standard,
			"restricted" : data.response[0].restricted,
			"productsAvailable" : data.response[0].productsAvailable
		});

		mainDB.add(model);

	}, function(error) {
		Ti.API.info("Api.getIndustries error: " + JSON.stringify(error));
	});

	Api.getContacts(function(data) {
		var model = Alloy.createModel('mainDB', {
			"type" : "Contacts",
			"lastname" : data.response[0].LastName,
			"firstname" : data.response[0].FirstName,
			"branch" : data.response[0].Branch,
			"product" : data.response[0].Product,
			"role" : data.response[0].Role,
			"title" : data.response[0].Title,
			"businessemailaddress" : data.response[0].BusinessEmailAddress,
			"telephonenumber" : data.response[0].TelephoneNumber
		});

		mainDB.add(model);

	}, function(error) {
		Ti.API.info("Api.getContacts error: " + JSON.stringify(error));
	});

	setTimeout(function() {
		$.activityInd.updateTitle({
			title : 'Done..'
		});
		$.activityInd.hide();
	}, 3000);
})();
