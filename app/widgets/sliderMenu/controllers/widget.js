var handlers = {
	'click' : '',
	'longpress' : ''
};

function onSelect(e) {
	$.menuView.visible = true;
	$.header.visible = false;
}

function onMenuViewHide() {
	$.menuView.visible = false;
	$.header.visible = true;
}

function loadScrollView(scrollview, content) {
	for (var i = 0,
	    view,
	    imgView,
	    bottombar,
	    viewblur,
	    lable; i < content.length; i++) {

		view = Ti.UI.createView({
			layout : 'vertical',
			top : '0',
			height : 150,
			width : 160
		});

		bottombar = Ti.UI.createView({
			top : 15,
			height : 65,
			backgroundColor : '#00000000'
		});

		viewblur = Ti.UI.createView({
			opacity : "0.2",
			backgroundColor : "#fafafa"
		});

		imgView = Ti.UI.createImageView({
			image : content[i].iconEnable,
			top : 10
		});

		lable = Ti.UI.createLabel({
			font : {
				fontSize : 14,
				fontFamily : 'Helvetica Neue'
			},
			color : 'white',
			left : 2,
			right : 2,
			opacity : 1,
			text : content[i].text,
			textAlign : Titanium.UI.TEXT_ALIGNMENT_CENTER,
			top : '20%'
		});

		bottombar.add(viewblur);
		bottombar.add(lable);
		view.add(imgView);
		view.add(bottombar);

		// Add event listner to view.
		view.addEventListener('click', viewClicked);
		view.addEventListener('longpress', viewLongpressed);

		scrollview.add(view);
	};
	return scrollview;
}

exports.initWidget = function(listeners, dropdownContent) {
	var scrollView1 = Ti.UI.createScrollView({
		top : '4%',
		layout : 'horizontal',
		height : '70%',
		disableBounce : 'true'
	});

	var scrollView2 = Ti.UI.createScrollView({
		top : '4%',
		layout : 'horizontal',
		height : '70%',
		disableBounce : 'true'
	});

	// Setup callback functions on view events.
	initWidgetEventListeners(listeners);
	// Populate the ScrollViews and add to its parent view.
	$.testView.add(loadScrollView(scrollView1, dropdownContent[0]));
	$.testView2.add(loadScrollView(scrollView2, dropdownContent[1]));
};

/*
 *   Event Handling
 * * * * * * * * * * * */

function viewClicked() {
	// View Animation on click
	Ti.API.info("click: animate view then callback.");
	handlers.viewClick();
}

function viewLongpressed() {
	// View Animation on longpress
	Ti.API.info("longPressed: animate view then callback.");
	handlers.viewLongpress();
}

/* Setup callback functions on view events. */
/* Accepts array of object [{'event','cbFunc'}] */
function initWidgetEventListeners(args) {
	_.each(args, function(item) {
		switch(item.event) {
		case 'click' :
			handlers.viewClick = item.cbFunc;
			break;

		case 'longpress' :
			handlers.viewLongpress = item.cbFunc;
			break;
		};
	});

};
 
// blur effect module call.
var UIBlurView = require('com.artanisdesign.uivisualeffect');
	var proxy2 = UIBlurView.createView({ 
		effect : 'dark', //extralight, dark
		width : Ti.UI.FILL,
		height : Ti.UI.FILL,
		top : 0,
		left : 0
	});
$.viewBlur.add(proxy2);