/**
 * The ActivityIndicator Widget controller generates an ActivityIndicitor, which is hidden by default
 *
 * @class sc.ActivityIndicator.controller.Widget
 */
'use strict';
var AIbottom; //cache the bottom passed in config during the construct and use it again in the updateTitle method

_.extend(this, {
	construct: function(config) {
		config = config || {};
		AIbottom = config.bottom; //cache it here
		$.activityIndicator.applyProperties({
			top: config.top,
			bottom: config.bottom,
			left: config.left,
			right: config.right,
			width: config.width,
			height: config.height
		});

		if (config.blueSpinner) {
			$.activityIndicator.image = WPATH('/images/blue-load_spinner.png');
		}

		$.container.applyProperties({
			backgroundColor: config.backgroundColor
		});

		if (config.visible) {
			$.show();
		}
	},
	/**
	 * Updates the label inside the AI
	 * example usage :
	 * $.AI.updateTitle({
	 *	  title: "Downloading\n490 of 500\n files"
	 * });
	 */
	updateTitle: function(config) {
		$.lblTitle.applyProperties({
			text: config.title,
			top: config.top,
			bottom: AIbottom + 50, //use the cached value
			left: config.left,
			right: config.right,
			width: config.width,
			height: config.height
		});
	},

	/**
	 * Show the ActivityIndicator
	 */
	show: function() {
		_keepAnimating = true;

		if ($.container.visible === false) {
			_spinAnimation.addEventListener('complete', onSpinIsComplete);
			$.activityIndicator.animate(_spinAnimation);

			$.container.visible = true;
		}
	},

	/**
	 * Hide the ActivityIndicator.
	 *
	 * This is done via an Animation fading out the ActivityIndicator on iOS and directly on Android
	 */
	hide: function() {
		if ($.container.visible === true) {

			_keepAnimating = false;

			// Fade out like native iOS activityIndicators
			if (OS_IOS) {
				_fadeOutAnimation.addEventListener('complete', onFadeOutIsComplete);

				$.container.animate(_fadeOutAnimation);
			}
			// Just hide
			else {
				$.container.visible = false;
			}
		}
	},

	/**
	 * Either show or hide the activityIndicator based on the boolean given
	 *
	 * @param {Boolan} bool True to shode, false to hide the activityIndicator
	 */
	setVisible: function(bool) {
		if (bool) {
			$.show();
		} else {
			$.hide();
		}
	}
});

/**
 * Animation used when hiding the ActivityIndicator
 * @private
 */
var _fadeOutAnimation = Ti.UI.createAnimation({
	curve: Ti.UI.ANIMATION_CURVE_EASE_IN_OUT,
	opacity: 0,
	duration: 300,
});

var _keepAnimating = true;

var _transform = Ti.UI.create2DMatrix().rotate(179);


var _spinAnimation = Titanium.UI.createAnimation({
	transform: _transform,
	duration: 3000, // chage this value to adjust rotation speed
	curve: Ti.UI.ANIMATION_CURVE_LINEAR
});

/**
 * Act on completion of the fade-out animation. Resetting the container into original configuration.
 *
 * @param {Object} evt Event details
 */
function onFadeOutIsComplete(evt) {
	_fadeOutAnimation.removeEventListener('complete', onFadeOutIsComplete);

	$.container.visible = false;
	$.container.opacity = 1;

	_keepAnimating = false;
}

function onSpinIsComplete(evt) {
	if (!_keepAnimating) {
		_spinAnimation.removeEventListener('complete', onSpinIsComplete);
		return false;
	}

	// First overwrite transform, so spinner keeps spinning
	_transform = _transform.rotate(179);

	// Assign new transformation
	_spinAnimation.transform = _transform;

	setTimeout(function() {
		// And do another animation
		$.activityIndicator.animate(_spinAnimation);
	},100);
}