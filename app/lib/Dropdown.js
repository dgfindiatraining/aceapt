'use strict';

module.exports = {
	getDropdownProducts : function() {
		var product = [{
			id : 0,
			type : 'Product',
			text : 'Accident & Health',
			iconEnable : 'images/accident_health_enable.png',
			iconActive : 'images/accident_health_active.png'
		}, {
			id : 1,
			type : 'Product',
			text : 'Boiler & Machinery',
			iconEnable : 'images/b&m_enable',
			iconActive : 'images/b&m_active'
		}, {
			id : 2,
			type : 'Product',
			text : 'Computer Hardware',
			iconEnable : 'images/computer_hardware_enable.png',
			iconActive : 'images/computer_hardware_active.png'
		}, {
			id : 3,
			type : 'Product',
			text : 'Construction',
			iconEnable : 'images/construction_enable.png',
			iconActive : 'images/construction_active.png'
		}, {
			id : 4,
			type : 'Product',
			text : 'Crime',
			iconEnable : 'images/crime_enable',
			iconActive : 'images/crime_active'
		}, {
			id : 5,
			type : 'Product',
			text : 'Cyber',
			iconEnable : 'images/cyber_enable',
			iconActive : 'images/cyber_active'
		}, {
			id : 6,
			type : 'Product',
			text : 'Financial Lines',
			iconEnable : 'images/construction_enable',
			iconActive : 'images/construction_active'
		}, {
			id : 7,
			type : 'Product',
			text : 'Enviromental Liability',
			iconEnable : 'images/environmental_enable.png',
			iconActive : 'images/environmental_active.png'
		}, {
			id : 8,
			type : 'Product',
			text : 'Financial Institutions',
			iconEnable : 'images/financial_institutions_enable.png',
			iconActive : 'images/financial_institutions_active.png'
		}, {
			id : 9,
			type : 'Product',
			text : 'Liability',
			iconEnable : 'images/liability_enable.png',
			iconActive : 'images/liability_active.png'
		}, {
			id : 10,
			type : 'Product',
			text : 'Marine',
			iconEnable : 'images/marine_enable.png',
			iconActive : 'images/marine_active.png'
		}, {
			id : 11,
			type : 'Product',
			text : 'Professional Indemnity',
			iconEnable : 'images/education_enable',
			iconActive : 'images/education_active'
		}, {
			id : 12,
			type : 'Product',
			text : 'Property',
			iconEnable : 'images/property_enable.png',
			iconActive : 'images/property_active.png'
		}, {
			id : 13,
			type : 'Product',
			text : 'Surety',
			iconEnable : 'images/surety_enable.png',
			iconActive : 'images/surety_active.png'
		}, {
			id : 14,
			type : 'Product',
			text : 'Terrorism',
			iconEnable : 'images/terrorism_enable.png',
			iconActive : 'images/terrorism_active.png'
		}];
		return product;
	},
	getDropdownIndustry : function() {
		var industry = [{
			id : 0,
			type : 'Industry',
			text : 'Administration and support services',
			iconEnable : 'images/administration_enable',
			iconActive : 'images/administration_active'
		}, {
			id : 1,
			type : 'Industry',
			text : 'Agriculture Forestry Fishing',
			iconEnable : 'images/agriculture_enable',
			iconActive : 'images/agriculture_active'
		}, {
			id : 2,
			type : 'Industry',
			text : 'Arts Entertainment & Recreation',
			iconEnable : 'images/art_entertainment_enable.png',
			iconActive : 'images/art_entertainment_active.png'
		}, {
			id : 3,
			type : 'Industry',
			text : 'Construction',
			iconEnable : 'images/construction_enable',
			iconActive : 'images/construction_active'
		}, {
			id : 4,
			type : 'Industry',
			text : 'Education',
			iconEnable : 'images/education_enable',
			iconActive : 'images/education_active'
		}, {
			id : 5,
			type : 'Industry',
			text : 'Finance & Insurance',
			iconEnable : 'images/finance_insurance_enable.png',
			iconActive : 'images/finance_insurance_active.png'
		}, {
			id : 6,
			type : 'Industry',
			text : 'Healthcare',
			iconEnable : 'images/construction_enable',
			iconActive : 'images/construction_active'
		}, {
			id : 7,
			type : 'Industry',
			text : 'Hotels & Restaurants',
			iconEnable : 'images/hotels_restaurants_enable.png',
			iconActive : 'images/hotels_restaurants_active.png'
		}, {
			id : 8,
			type : 'Industry',
			text : 'Information & Communication',
			iconEnable : 'images/info_communication_enable.png',
			iconActive : 'images/info_communication_active.png'
		},{
			id : 9,
			type : 'Industry',
			text : 'Manufacturing',
			iconEnable : 'images/manufacturing_enable.png',
			iconActive : 'images/manufacturing_active.png'
		}];
		return industry;
	}
};