/**
 * File takes care of all the API mappings for the app
 */
'use strict';

var XHR = require('thirdParty/xhr'),
    serverUrl = Alloy.CFG.serverUrl,
    api = {
	getProducts : serverUrl + '/api/v1/readProductExcel',
	getIndustries : serverUrl + '/api/v1/readIndustryExcel',
	getContacts : serverUrl + '/api/v1/readContactsExcel'
};

module.exports = {
	getProducts : function(success, error) {
		var xhr = new XHR();
		xhr.get(api.getProducts, function(e) {
			var data = JSON.parse(e.data);
			if (success)
				success(data);
			// jshint ignore: line
		}, function(e) {
			if (error)
				error(e);
			// jshint ignore: line
		}, {
			username : Alloy.CFG.basicAuthUsername,
			password : Alloy.CFG.basicAuthPassword,
			contentType : 'application/json'
		});
	},
	getIndustries : function(success, error) {
		var xhr = new XHR();
		xhr.get(api.getIndustries, function(e) {
			var data = JSON.parse(e.data);
			if (success)
				success(data);
			// jshint ignore: line
		}, function(e) {
			if (error)
				error(e);
			// jshint ignore: line
		}, {
			username : Alloy.CFG.basicAuthUsername,
			password : Alloy.CFG.basicAuthPassword,
			contentType : 'application/json'
		});
	},
	getContacts : function(success, error) {
		var xhr = new XHR();
		xhr.get(api.getContacts, function(e) {
			var data = JSON.parse(e.data);
			if (success)
				success(data);
			// jshint ignore: line
		}, function(e) {
			if (error)
				error(e);
			// jshint ignore: line
		}, {
			username : Alloy.CFG.basicAuthUsername,
			password : Alloy.CFG.basicAuthPassword,
			contentType : 'application/json'
		});
	}
}; 