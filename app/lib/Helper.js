'use strict';

module.exports = {
	setProducts : function(products) {
		Ti.App.Properties.setObject('PRODUCTS', products);
	},
	getProducts : function() {
		return Ti.App.Properties.getObject('PRODUCTS', {});
	},
	setIndustries : function(products) {
		Ti.App.Properties.setObject('INDUSTRIES', products);
	},
	getIndustries : function() {
		return Ti.App.Properties.getObject('INDUSTRIES', {});
	},
	setContacts : function(products) {
		Ti.App.Properties.setObject('CONTACTS', products);
	},
	getContacts : function() {
		return Ti.App.Properties.getObject('CONTACTS', {});
	}
};
